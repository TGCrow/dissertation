using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TerrainManager : MonoBehaviour
{
    [SerializeField]
    private GameObject TerrainPrefab;
    private Dictionary<Vector3, GameObject> ActiveChunks;

    void spawnChunksForMarchingCube()
    {
        int currentXind = Mathf.RoundToInt(transform.position.x / (Presets.chunkRadius * 2));
        int currentZind = Mathf.RoundToInt(transform.position.z / (Presets.chunkRadius * 2));
        int currentYind = Mathf.RoundToInt(transform.position.y / (Presets.chunkRadius * 2));
        int chunkViewRadius = Presets.chunksViewRadius;
        Vector3 position = Vector3.zero;
        for (int x = -chunkViewRadius; x <= chunkViewRadius; x++)
        {
            for (int y = -chunkViewRadius; y <= chunkViewRadius; y++)
            {
                for (int z = -chunkViewRadius; z <= chunkViewRadius; z++)
                {
                    position.x = (currentXind + x) * (Presets.chunkRadius * 2);
                    position.y = (currentYind + y) * (Presets.chunkRadius * 2);
                    position.z = (currentZind + z) * (Presets.chunkRadius * 2);
                    if (!ActiveChunks.ContainsKey(position))
                    {
                        ActiveChunks.Add(position, Instantiate(TerrainPrefab, position, Quaternion.identity));
                    }
                }
            }
        }
    }
    void spawnChunksForHeightmap()
    {
        int currentXind = Mathf.RoundToInt(transform.position.x / (Presets.chunkRadius * 2));
        int currentZind = Mathf.RoundToInt(transform.position.z / (Presets.chunkRadius * 2));
        int chunkViewRadius = Presets.chunksViewRadius;
        Vector3 position = Vector3.zero;
        for (int x = -chunkViewRadius; x <= chunkViewRadius; x++)
        {
            for (int z = -chunkViewRadius; z <= chunkViewRadius; z++)
            {
                position.x = (currentXind + x) * (Presets.chunkRadius * 2);
                position.z = (currentZind + z) * (Presets.chunkRadius * 2);
                if (!ActiveChunks.ContainsKey(position))
                {
                    ActiveChunks.Add(position, Instantiate(TerrainPrefab, position, Quaternion.identity));
                }
            }
        }
    }

    void removeOutOfRangeChunks()
    {
        Vector3 position = Vector3.zero;
        List<Vector3> ChunksToRemove = new List<Vector3>();
        foreach (var chunk in ActiveChunks)
        {
            position = chunk.Key;
            if (Mathf.Abs(Vector3.Distance(position, transform.position)) > Presets.chunkRadius * Presets.chunksViewRadius * 4)
            {
                ChunksToRemove.Add(position);
            }
        }

        for (int i = 0; i < ChunksToRemove.Count; i++)
        {
            position = ChunksToRemove[i];
            GameObject chunkToRemove = ActiveChunks[position];
            ActiveChunks.Remove(position);
            if (Presets.useMarchingCube)
            {
                chunkToRemove.GetComponent<TerrainGenerator>().isDespawned = true;
            }
            else
            {
                GameObject.Destroy(chunkToRemove);
            }
        }

        ChunksToRemove.Clear();
    }

    // Start is called before the first frame update
    void Awake()
    {
        Presets.seed = System.DateTime.UtcNow.Millisecond;
        ActiveChunks = new Dictionary<Vector3, GameObject>();
        if (Presets.useMarchingCube)
        {
            this.spawnChunksForMarchingCube();
        }
        else
        {
            this.spawnChunksForHeightmap();
        }
        
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Presets.useMarchingCube)
        {
            this.spawnChunksForMarchingCube();
        }
        else
        {
            this.spawnChunksForHeightmap();
        }
        this.removeOutOfRangeChunks();

        
    }

    public void regenerateTerrain()
    {
        List<Vector3> ChunksToRemove = new List<Vector3>();
        Vector3 position;
        foreach (var chunk in ActiveChunks)
        {
            position = chunk.Key;
            
            ChunksToRemove.Add(position);
            
        }
        for (int i = 0; i < ChunksToRemove.Count; i++)
        {
            position = ChunksToRemove[i];
            GameObject chunkToRemove = ActiveChunks[position];
            ActiveChunks.Remove(position);
            if (Presets.useMarchingCube)
            {
                chunkToRemove.GetComponent<TerrainGenerator>().isDespawned = true;
            }
            else
            {
                GameObject.Destroy(chunkToRemove);
            }

        }

       

        ChunksToRemove.Clear();
    }

    public void regenColours()
    {
        Vector3 position;
        foreach (var chunk in ActiveChunks)
        {
            position = chunk.Key;

            ActiveChunks[position].GetComponent<TerrainGenerator>().regenColours();

        }
    }



}
