using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Jobs;
using Unity.Collections;

public static class MarchingCubeMesh
{
    public static float interpilateVal(float val, float maxval)
    {
        return val / maxval;
    }

    
    public static Mesh generateMesh(Vector3 center){

        Random.InitState(Presets.seed);
        NativeArray<int> xoffsets = new NativeArray<int>(Presets.octives, Allocator.TempJob);
        NativeArray<int> yoffsets = new NativeArray<int>(Presets.octives, Allocator.TempJob);
        NativeArray<int> zoffsets = new NativeArray<int>(Presets.octives, Allocator.TempJob);
        List<Vector3> verticies = new List<Vector3>();
        List<int> triangles = new List<int>();

        for (int i = 0; i < Presets.octives; i++)
        {
            xoffsets[i] = Random.Range(-100000, 100000);
            yoffsets[i] = Random.Range(-100000, 100000);
            zoffsets[i] = Random.Range(-100000, 100000);
            //offsets[i] = new Vector3(xoffset, yoffset,zoffset);
        }
        for (int i = 0; i < Presets.octives; i++)
        {
            int xoffset = Random.Range(-100000, 100000);
            int yoffset = Random.Range(-100000, 100000);
            int zoffset = Random.Range(-100000, 100000);
            //offsets2[i] = new Vector3(xoffset, yoffset, zoffset);
        }

        float distanceBetweenPoints = ((float)Presets.chunkRadius / (float)Presets.fidelity);
        int sizeofside = Presets.fidelity * 2 + 1;
        int sizeofjob = sizeofside * sizeofside * sizeofside;
        NativeArray<Vector4> jobPoints = new NativeArray<Vector4>(sizeofjob, Allocator.TempJob);
        var job = new pointGeneration()
        {
            xoffsets = xoffsets,
            yoffsets = yoffsets,
            zoffsets = zoffsets,
            octives = Presets.octives,
            variation = Presets.variation,
            octiveRoughnessScaler = Presets.octiveRoughnesScaler,
            octiveScaleScaler = Presets.octivesScalerScaler,
            sizeoOfSide = sizeofside,
            distanceBetweenPoints = distanceBetweenPoints,
            Points = jobPoints,
            center = center
        };

        JobHandle handle = job.Schedule(sizeofjob, 64);
        handle.Complete();

        Vector4[,,] Points = new Vector4[sizeofside, sizeofside, sizeofside];

        for(int x = 0; x < sizeofside; x++)
        {
            for (int y = 0; y < sizeofside; y++)
            {
                for (int z = 0; z < sizeofside; z++)
                {
                    Points[x, y, z] = job.Points[x + y * sizeofside + z * sizeofside * sizeofside];
                }
            }
        }

        xoffsets.Dispose();
        yoffsets.Dispose();
        zoffsets.Dispose();
        jobPoints.Dispose();

        /*Vector4[,,] Points = new Vector4[Presets.fidelity * 2+1, Presets.fidelity * 2+1, Presets.fidelity * 2+1];
        for (int x = -Presets.fidelity; x <= Presets.fidelity; x++)
        {
            for (int y = -Presets.fidelity; y <= Presets.fidelity; y++)
            {
                for (int z = -Presets.fidelity; z <= Presets.fidelity; z++)
                {
                    float xpos= (x * distanceBetweenPoints);
                    float zpos= (z * distanceBetweenPoints);
                    float ypos= (y * distanceBetweenPoints);
                    float wval = 0;
                    float octiveRoughness = Presets.variation;
                    int octiveScale = 1;
                    float initialGroundLevel = 0;

                    float wval2 = 0;

                    for (int i = 0; i < Presets.octives; i++)
                    {

                        
                        float noiseX = (xpos + center.x + offsets[i].x)/octiveRoughness;
                        float noiseY = (ypos + center.y + offsets[i].y) / octiveRoughness;
                        float noisez = (zpos + center.z + offsets[i].z) / octiveRoughness;
                        float noiseX2 = (xpos + center.x + offsets2[i].x) / octiveRoughness;
                        float noiseY2 = (ypos + center.y + offsets2[i].y) / octiveRoughness;
                        float noisez2 = (zpos + center.z + offsets2[i].z) / octiveRoughness;
                        initialGroundLevel += Mathf.PerlinNoise(noiseX,noisez)/octiveScale;

                        float octiveVal = Mathf.PerlinNoise(noiseX, noiseY)*2 -1;
                        octiveVal += Mathf.PerlinNoise(noiseX, noisez) * 2 - 1;
                        octiveVal += Mathf.PerlinNoise(noiseY, noisez) * 2 - 1;

                        octiveVal += Mathf.PerlinNoise(noiseY, noiseX) * 2 - 1;
                        octiveVal += Mathf.PerlinNoise(noisez, noiseX) * 2 - 1;
                        octiveVal += Mathf.PerlinNoise(noisez, noiseY) * 2 - 1;

                        float octiveVal2 = Mathf.PerlinNoise(noiseX2, noiseY2) * 2 - 1;
                        octiveVal2 += Mathf.PerlinNoise(noiseX2, noisez2) * 2 - 1;
                        octiveVal2 += Mathf.PerlinNoise(noiseY2, noisez2) * 2 - 1;

                        octiveVal2 += Mathf.PerlinNoise(noiseY2, noiseX2) * 2 - 1;
                        octiveVal2 += Mathf.PerlinNoise(noisez2, noiseX2) * 2 - 1;
                        octiveVal2 += Mathf.PerlinNoise(noisez2, noiseY2) * 2 - 1;

                        wval += (octiveVal / 6.0f) / octiveScale;
                        wval2 += (octiveVal2 / 6.0f) / octiveScale;


                        octiveRoughness = octiveRoughness * Presets.octiveRoughnesScaler;
                        octiveScale = octiveScale * Presets.octivesScalerScaler;
                    }
                    
                    initialGroundLevel = Mathf.Pow(initialGroundLevel, Presets.terrainSteepness) * Presets.scale;
                    float maxGround = initialGroundLevel * 1.4f;
                    float minGround = initialGroundLevel * 0.1f;
                    float terrainVal = center.y + ypos - initialGroundLevel;
                    if (center.y+ ypos < maxGround &&center.y+ ypos>minGround) terrainVal = wval + wval2;
                    //terrainVal = (wval+wval2) * Presets.marchingCubeOffsetScale;
                    Points[x+Presets.fidelity,y+Presets.fidelity,z+Presets.fidelity] = new Vector4(xpos,ypos,zpos, terrainVal);
                }
            }
        }*/

        Vector4[] cubeVerticies = new Vector4[8];
        for (int x =0; x <= Presets.fidelity*2-1; x++)
        {
            for (int y = 0; y <= Presets.fidelity*2 - 1; y++)
            {
                for (int z = 0; z <= Presets.fidelity*2 - 1; z++)
                {
                    cubeVerticies[0] = Points[x,y,z];
                    cubeVerticies[1] = Points[x+1,y,z];
                    cubeVerticies[2] = Points[x+1,y,z+1];
                    cubeVerticies[3] = Points[x,y,z+1];
                    cubeVerticies[4] = Points[x,y+1,z];
                    cubeVerticies[5] = Points[x+1,y+1,z];
                    cubeVerticies[6] = Points[x+1,y+1,z+1];
                    cubeVerticies[7] = Points[x,y+1,z+1];
                    int meshIndex = 0;
                    for (int i = 0; i < 8; i++)
                    {
                        if (cubeVerticies[i].w < Presets.marchingCubeGroundHeight)
                        {
                            meshIndex |= 1 << i;
                        }
                    }

                    

                    for (int i = 0; i < 5; i++)
                    {
                        if (MarchingCubeTriangulations.triangulation[meshIndex, i * 3] < 0)
                        {
                            break;
                        }
                        for(int j = 0; j < 3; j++)
                        {
                            Vector4 A = cubeVerticies[MarchingCubeTriangulations.cornerIndexAFromEdge[MarchingCubeTriangulations.triangulation[meshIndex, i * 3 + j]]];
                            Vector4 B = cubeVerticies[MarchingCubeTriangulations.cornerIndexBFromEdge[MarchingCubeTriangulations.triangulation[meshIndex, i * 3 + j]]];
                            //Vector3 vertex = interpolateAB(A,B);
                            Vector3 vertex = A + (B - A) / 2;
                            //Debug.Log(A + "," + B + "," + vertex);
                            verticies.Add(vertex);
                            triangles.Add(verticies.Count - 1);
                        }
                    }
                }
            }
        }

        Mesh mesh = new Mesh();
        mesh.vertices = verticies.ToArray();
        mesh.triangles = triangles.ToArray();
        mesh.RecalculateNormals();
        return mesh;
    }

    public static Vector3 interpolateAB(Vector4 A,Vector4 B)
    {
        Vector3 aVertex = A;
        Vector3 bVertex = B;
        float t = (Presets.marchingCubeGroundHeight - A.w) / (B.w - A.w);
        return aVertex + t * (bVertex - aVertex);

    }

    

    public struct meshGenerator : IJob
    {
        [ReadOnly]
        public int sizeOfSide;
        [ReadOnly]
        public NativeArray<Vector4> Points;
        public NativeList<Vector3> verticies;
        public NativeList<int> triangles;

        public void Execute()
        {
            
            NativeArray<Vector4> cubeVerticies = new NativeArray<Vector4>(8,Allocator.Temp);
            for (int x = 0; x <= Presets.fidelity * 2 - 1; x++)
            {
                for (int y = 0; y <= Presets.fidelity * 2 - 1; y++)
                {
                    for (int z = 0; z <= Presets.fidelity * 2 - 1; z++)
                    {
                        cubeVerticies[0] = Points[x + y * sizeOfSide + z * sizeOfSide * sizeOfSide];
                        cubeVerticies[1] = Points[x + 1 + y * sizeOfSide + z * sizeOfSide * sizeOfSide];
                        cubeVerticies[2] = Points[x + 1 + y * sizeOfSide + (z + 1) * sizeOfSide * sizeOfSide];
                        cubeVerticies[3] = Points[x + y * sizeOfSide + (z + 1) * sizeOfSide * sizeOfSide];
                        cubeVerticies[4] = Points[x + (y + 1) * sizeOfSide + z * sizeOfSide * sizeOfSide];
                        cubeVerticies[5] = Points[x + 1 + (y + 1) * sizeOfSide + z * sizeOfSide * sizeOfSide];
                        cubeVerticies[6] = Points[x + 1 + (y + 1) * sizeOfSide + (z + 1) * sizeOfSide * sizeOfSide];
                        cubeVerticies[7] = Points[x + (y + 1) * sizeOfSide + (z + 1) * sizeOfSide * sizeOfSide];

                        int meshIndex = 0;


                        for (int i = 0; i < 8; i++)
                        {
                            if (cubeVerticies[i].w < Presets.marchingCubeGroundHeight)
                            {
                                meshIndex |= 1 << i;
                            }
                        }



                        for (int i = 0; i < 5; i++)
                        {
                            if (MarchingCubeTriangulations.triangulation[meshIndex, i * 3] < 0)
                            {
                                break;
                            }
                            for (int j = 0; j < 3; j++)
                            {
                                Vector4 A = cubeVerticies[MarchingCubeTriangulations.cornerIndexAFromEdge[MarchingCubeTriangulations.triangulation[meshIndex, i * 3 + j]]];
                                Vector4 B = cubeVerticies[MarchingCubeTriangulations.cornerIndexBFromEdge[MarchingCubeTriangulations.triangulation[meshIndex, i * 3 + j]]];
                                Vector3 vertex = interpolateAB(A, B);
                                //Vector3 vertex = A + (B - A) / 2;
                                //Debug.Log(A + "," + B + "," + vertex);
                                if(!verticies.Contains(vertex))verticies.Add(vertex);
                                triangles.Add(verticies.IndexOf(vertex));
                            }
                        }

                    }
                }
            }

        }
    }

    public struct pointGeneration : IJobParallelFor
    {
        [ReadOnly]
        public NativeArray<int> xoffsets;
        [ReadOnly]
        public NativeArray<int> yoffsets;
        [ReadOnly]
        public NativeArray<int> zoffsets;
        [ReadOnly]
        public int octives;
        [ReadOnly]
        public float variation;
        [ReadOnly]
        public float octiveRoughnessScaler;
        [ReadOnly]
        public int octiveScaleScaler;
        [ReadOnly]
        public int sizeoOfSide;
        [ReadOnly]
        public float distanceBetweenPoints;
        [ReadOnly]
        public Vector3 center;

        public NativeArray<Vector4> Points;


        public void Execute(int index)
        {
            int xIndex = (index % (sizeoOfSide * sizeoOfSide)) % sizeoOfSide;
            int yIndex = ((index % (sizeoOfSide * sizeoOfSide)) - xIndex) / sizeoOfSide;
            int zIndex = (index - (yIndex * sizeoOfSide) - xIndex) / (sizeoOfSide * sizeoOfSide);

            float xpos = (xIndex-(sizeoOfSide-1)/variation) * distanceBetweenPoints;
            float zpos = (zIndex-(sizeoOfSide-1)/variation) * distanceBetweenPoints;
            float ypos = (yIndex-(sizeoOfSide-1)/variation) * distanceBetweenPoints;
            float wval = 0;
            float octiveRoughness = variation;
            int octiveScale = 1;
            float initialGroundLevel = 0;

            for (int i = 0; i < octives; i++)
            {


                float noiseX = (xpos + center.x + xoffsets[i]) / octiveRoughness;
                float noiseY = (ypos + center.y + yoffsets[i]) / octiveRoughness;
                float noisez = (zpos + center.z + zoffsets[i]) / octiveRoughness;
                initialGroundLevel += Mathf.PerlinNoise(noiseX, noisez) / (float)octiveScale;

                float octiveVal = Mathf.PerlinNoise(noiseX, noiseY) * 2 - 1;
                octiveVal += Mathf.PerlinNoise(noiseX, noisez) * 2 - 1;
                octiveVal += Mathf.PerlinNoise(noiseY, noisez) * 2 - 1;

                octiveVal += Mathf.PerlinNoise(noiseY, noiseX) * 2 - 1;
                octiveVal += Mathf.PerlinNoise(noisez, noiseX) * 2 - 1;
                octiveVal += Mathf.PerlinNoise(noisez, noiseY) * 2 - 1;


                wval += (octiveVal / 6.0f) / (float)octiveScale;


                octiveRoughness = octiveRoughness * octiveRoughnessScaler;
                octiveScale = octiveScale * octiveScaleScaler;

            }
            initialGroundLevel = Mathf.Pow(initialGroundLevel, Presets.terrainSteepness);
            float scalledInitialGroundLevel = initialGroundLevel * (float)Presets.scale;
            float hightpercent = (center.y + ypos) / scalledInitialGroundLevel;
            float terrainVal = 0;
            if (center.y + ypos > 0.0f)
            {
                terrainVal = wval * (1 - hightpercent) + (center.y + ypos) * hightpercent * hightpercent;
            }
            else
            {
                terrainVal = wval;
            }
            /*float scalledInitialGroundLevel = initialGroundLevel * (float)Presets.scale;
            float maxGround = scalledInitialGroundLevel * 1.4f;
            float terrainVal = center.y + ypos - scalledInitialGroundLevel;
            float interpAmount = 1-Mathf.Clamp01(Mathf.Abs(terrainVal) / initialGroundLevel * 0.4f);
            terrainVal = Mathf.Clamp(terrainVal, -distanceBetweenPoints, distanceBetweenPoints*3);

            if (center.y + ypos < maxGround && center.y + ypos > scalledInitialGroundLevel)
            {
                terrainVal += wval *  distanceBetweenPoints;
            }

            else if (center.y + ypos <= scalledInitialGroundLevel)
            {

               terrainVal += wval*distanceBetweenPoints;
            }*/
           
            Points[index] = new Vector4(xpos, ypos, zpos, terrainVal);

        }
    }

    public struct calculateUVs : IJobParallelFor
    {
        [ReadOnly]
        public NativeArray<Vector3> verticies;
        public NativeArray<Vector2> UVs;
        public float centerheight;
        public void Execute(int index)
        {
            float height = verticies[index].y + centerheight;
            float gradientPos = 0;
            if (height < Presets.grassColourStartHeight)
            {
                gradientPos = Mathf.Clamp(Mathf.Abs((height - Presets.grassColourStartHeight)) / 4,0.0f,0.5f)+0.49f;
            }
            else if (height < Presets.mountainColourStartHeight)
            {
                gradientPos = Mathf.Clamp((height-Presets.grassColourStartHeight) /(Presets.mountainColourStartHeight- Presets.grassColourStartHeight), 0.0f, 0.5f);
            }
            else if (height < Presets.mountainColourEndgreadientHeight)
            {
                gradientPos = Mathf.Clamp((height - Presets.mountainColourStartHeight) / (Presets.mountainColourEndgreadientHeight - Presets.mountainColourStartHeight), 0.0f, 0.5f) +0.49f;
            }
            else
            {
                gradientPos = 0.99f;
            }

            UVs[index] = new Vector2(gradientPos, 0.5f);
        }
    }
}
