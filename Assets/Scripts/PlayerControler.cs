using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerControler : MonoBehaviour
{
    public float MoveSpeed = 1;
    public float rotationSpeed = 1;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        Vector3 moveDirection = Vector3.zero;
        
        if (Input.GetKey(KeyCode.W))
        {
            moveDirection += transform.forward;
        }
        if (Input.GetKey(KeyCode.S))
        {
            moveDirection += -transform.forward;
        }
        if(Input.GetKey(KeyCode.A))
        {
            transform.localEulerAngles += -Vector3.up * rotationSpeed * Time.deltaTime;
        }
        if(Input.GetKey(KeyCode.D))
        {
            transform.localEulerAngles += Vector3.up * rotationSpeed * Time.deltaTime;
        }
        if (Input.GetKey(KeyCode.LeftShift))
        {
            moveDirection += Vector3.up;
        }
        if (Input.GetKey(KeyCode.LeftControl))
        {
            moveDirection += -Vector3.up;
        }
        if (Input.GetKey(KeyCode.F))
        {
            transform.localEulerAngles += Vector3.right * rotationSpeed * Time.deltaTime;
        }
        if (Input.GetKey(KeyCode.E))
        {
            transform.localEulerAngles += -Vector3.right * rotationSpeed * Time.deltaTime;
        }
        moveDirection = moveDirection.normalized;
        transform.position += moveDirection * MoveSpeed * Time.deltaTime;
    }
}
