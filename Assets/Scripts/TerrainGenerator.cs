using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Jobs;
using Unity.Collections;

public class TerrainGenerator : MonoBehaviour
{
    private MarchingCubeMesh.pointGeneration job;
    private JobHandle handle;
    JobHandle handle2;
    private bool isMeshGenerated = false;
    private bool isPointsGenerated = false;
    private int sizeofside;
    public bool isDespawned = false;
    MarchingCubeMesh.meshGenerator job2;
    MarchingCubeMesh.calculateUVs job3;
    JobHandle handle3;
    NativeArray<int> xoffsets;
    NativeArray<int> yoffsets;
    NativeArray<int> zoffsets;
    NativeArray<Vector4> jobPoints;
    NativeList<Vector3> verticies;
    NativeList<int> triangles;
    NativeArray<Vector2> uvs;
    private bool isColoursGenerated = false;
    private bool regeningColours = false;
    // Start is called before the first frame update
    void Start()
    {
        if (Presets.useMarchingCube)
        {
            GeneratePoints();
        }
        else
        {
            GetComponent<MeshFilter>().mesh = PerlinNoiseMesh.GenerateMesh(transform.position);
        }
        
        
        

    }

    void GeneratePoints()
    {
        Random.InitState(Presets.seed);
        xoffsets = new NativeArray<int>(Presets.octives, Allocator.Persistent);
        yoffsets = new NativeArray<int>(Presets.octives, Allocator.Persistent);
        zoffsets = new NativeArray<int>(Presets.octives, Allocator.Persistent);
        List<Vector3> verticies = new List<Vector3>();
        List<int> triangles = new List<int>();

        for (int i = 0; i < Presets.octives; i++)
        {
            xoffsets[i] = Random.Range(-100000, 100000);
            yoffsets[i] = Random.Range(-100000, 100000);
            zoffsets[i] = Random.Range(-100000, 100000);
            //offsets[i] = new Vector3(xoffset, yoffset,zoffset);
        }
        for (int i = 0; i < Presets.octives; i++)
        {
            int xoffset = Random.Range(-100000, 100000);
            int yoffset = Random.Range(-100000, 100000);
            int zoffset = Random.Range(-100000, 100000);
            //offsets2[i] = new Vector3(xoffset, yoffset, zoffset);
        }

        float distanceBetweenPoints = ((float)Presets.chunkRadius / (float)Presets.fidelity);
        sizeofside = Presets.fidelity * 2 + 1;
        int sizeofjob = sizeofside * sizeofside * sizeofside;
        jobPoints = new NativeArray<Vector4>(sizeofjob, Allocator.Persistent);
        job = new MarchingCubeMesh.pointGeneration()
        {
            xoffsets = xoffsets,
            yoffsets = yoffsets,
            zoffsets = zoffsets,
            octives = Presets.octives,
            variation = Presets.variation,
            octiveRoughnessScaler = Presets.octiveRoughnesScaler,
            octiveScaleScaler = Presets.octivesScalerScaler,
            sizeoOfSide = sizeofside,
            distanceBetweenPoints = distanceBetweenPoints,
            Points = jobPoints,
            center = transform.position
        };

        handle = job.Schedule(sizeofjob, 64);
        
    }
    public void regenColours()
    {
        if (Presets.useMarchingCube)
        {
            NativeArray<Vector2> jobuvs = new NativeArray<Vector2>(GetComponent<MeshFilter>().mesh.vertices.Length, Allocator.Persistent);
            NativeArray<Vector3> jobverts = new NativeArray<Vector3>(GetComponent<MeshFilter>().mesh.vertices.Length, Allocator.Persistent);
            jobverts.CopyFrom(GetComponent<MeshFilter>().mesh.vertices);
            job3 = new MarchingCubeMesh.calculateUVs()
            {
                verticies = jobverts,
                UVs = jobuvs,
                centerheight = transform.position.y
            };
            handle3 = job3.Schedule(jobverts.Length, 64);
            regeningColours = true;
            Debug.Log("regening colours");
        }
        else
        {
            GetComponent<MeshFilter>().mesh.uv = PerlinNoiseMesh.getNewUvs(GetComponent<MeshFilter>().mesh.vertices);
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (regeningColours)
        {
            if (handle3.IsCompleted)
            {
                handle3.Complete();
                GetComponent<MeshFilter>().mesh.uv = job3.UVs.ToArray();
                job3.verticies.Dispose();
                job3.UVs.Dispose();
                regeningColours = false;
                Debug.Log("colours Regened");
            }
        }
        if (isMeshGenerated)
        {
            if (isDespawned)
            {
                Destroy(this.gameObject);
            }
            return;
        }
        if (!Presets.useMarchingCube)
        {
            return;
        }
        if (!handle.IsCompleted)
        {
            return;
        }
        
        

        if (!isPointsGenerated)
        {
            handle.Complete();

            if (isDespawned)
            {
                xoffsets.Dispose();
                yoffsets.Dispose();
                zoffsets.Dispose();
                jobPoints.Dispose();
                Destroy(this.gameObject);
                return;
            }

            verticies = new NativeList<Vector3>(Allocator.Persistent);
            triangles = new NativeList<int>(Allocator.Persistent);
            job2 = new MarchingCubeMesh.meshGenerator()
            {
                sizeOfSide = sizeofside,
                Points = jobPoints,
                verticies = verticies,
                triangles = triangles
            };

            xoffsets.Dispose();
            yoffsets.Dispose();
            zoffsets.Dispose();

            isPointsGenerated = true;
            handle2 = job2.Schedule();

            
        }

        if (!handle2.IsCompleted)
        {
            return;
        }
        handle2.Complete();

        if (isDespawned)
        {
            verticies.Dispose();
            triangles.Dispose();
            jobPoints.Dispose();
            Destroy(this.gameObject);
            return;
        }
        

        if (!isColoursGenerated)
        {
            uvs = new NativeArray<Vector2>(verticies.Length, Allocator.Persistent);
            NativeArray<Vector3> jobverts = new NativeArray<Vector3>(verticies.Length, Allocator.Persistent);
            jobverts.CopyFrom(verticies.ToArray());
            job3 = new MarchingCubeMesh.calculateUVs()
            {
                verticies = jobverts,
                UVs = uvs,
                centerheight = transform.position.y
            };
            handle3 = job3.Schedule(jobverts.Length,64);
            isColoursGenerated = true;
        }
        if (!handle3.IsCompleted)
        {
            return;
        }

        handle3.Complete();

        Mesh mesh = new Mesh();
        mesh.vertices = job2.verticies.ToArray();
        mesh.triangles = job2.triangles.ToArray();
        mesh.uv = uvs.ToArray();
        mesh.RecalculateNormals();
        GetComponent<MeshFilter>().mesh = mesh;
        isMeshGenerated = true;
        verticies.Dispose();
        triangles.Dispose();
        jobPoints.Dispose();
        job3.verticies.Dispose();
        uvs.Dispose();
    }

}
