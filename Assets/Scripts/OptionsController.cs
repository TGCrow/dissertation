using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class OptionsController : MonoBehaviour
{
    public TerrainManager terrainManager;
    public InputField scaleInput;
    public InputField chunkRadiusInput;
    public InputField ChunkViewRadiusInput;
    public InputField fidelityInput;
    public InputField variationInput;
    public InputField seedInput;
    public InputField OctivesInput;
    public InputField OctiveFrequencyInput;
    public InputField octiveScaleInput;
    public InputField terrainSteepnessInput;
    public InputField MarchingCubeGroundHeightInput;
    public InputField grassColourStartHeightInput;
    public InputField mountainColourStartHeightInput;
    public InputField mountainColourEndgreadientHeightInput;
    public Toggle UseMarchingcubeInput;
    public Canvas canvas;
    private bool showOptions = false;
    //public GameObject scaleInput;

    void Awake()
    {
        canvas.enabled = showOptions;
    }
    // Start is called before the first frame update
    void Start()
    {
        scaleInput.text = Presets.scale.ToString();        
        chunkRadiusInput.text = Presets.chunkRadius.ToString();
        ChunkViewRadiusInput.text = Presets.chunksViewRadius.ToString();
        fidelityInput.text = Presets.fidelity.ToString();
        variationInput.text = Presets.variation.ToString();
        seedInput.text = Presets.seed.ToString();
        OctivesInput.text = Presets.octives.ToString();
        OctiveFrequencyInput.text = Presets.octiveRoughnesScaler.ToString();
        octiveScaleInput.text = Presets.octivesScalerScaler.ToString();
        terrainSteepnessInput.text = Presets.terrainSteepness.ToString();
        MarchingCubeGroundHeightInput.text = Presets.marchingCubeGroundHeight.ToString();
        UseMarchingcubeInput.isOn = Presets.useMarchingCube;
        grassColourStartHeightInput.text = Presets.grassColourStartHeight.ToString();
        mountainColourStartHeightInput.text = Presets.mountainColourStartHeight.ToString();
        mountainColourEndgreadientHeightInput.text = Presets.mountainColourEndgreadientHeight.ToString();    
    }

    public void MarchingCubeToggle()
    {
        if (UseMarchingcubeInput.isOn)
        {
            setMarchingCubePresets();
        }
        else
        {
            setPerlinNoisePresets();
        }
    }

    void setMarchingCubePresets()
    {
        scaleInput.text = "100";
        chunkRadiusInput.text = "10";
        ChunkViewRadiusInput.text = "3";
        fidelityInput.text = "20";
        variationInput.text = "20";
        OctivesInput.text = "4";
        OctiveFrequencyInput.text = "0.3";
        octiveScaleInput.text = "6";
        terrainSteepnessInput.text = "2.5";
        MarchingCubeGroundHeightInput.text = "0.1";
    }

    void setPerlinNoisePresets()
    {
        scaleInput.text = "10";
        chunkRadiusInput.text = "10";
        ChunkViewRadiusInput.text = "4";
        fidelityInput.text = "40";
        variationInput.text = "20";
        OctivesInput.text = "3";
        OctiveFrequencyInput.text = "0.3";
        octiveScaleInput.text = "6";
        terrainSteepnessInput.text = "4";
    }

    public void regenerateTerrain()
    {
        Presets.scale = int.Parse(scaleInput.text);
        Presets.chunkRadius = int.Parse(chunkRadiusInput.text);
        Presets.chunksViewRadius = int.Parse(ChunkViewRadiusInput.text);
        Presets.fidelity = int.Parse(fidelityInput.text);
        Presets.variation = float.Parse(variationInput.text);
        Presets.seed = int.Parse(seedInput.text);
        Presets.octives = int.Parse(OctivesInput.text);
        Presets.octiveRoughnesScaler = float.Parse(OctiveFrequencyInput.text);
        Presets.octivesScalerScaler = int.Parse(octiveScaleInput.text);
        Presets.terrainSteepness = float.Parse(terrainSteepnessInput.text);
        Presets.marchingCubeGroundHeight = float.Parse(MarchingCubeGroundHeightInput.text);
        Presets.grassColourStartHeight = float.Parse(grassColourStartHeightInput.text);
        Presets.mountainColourStartHeight = float.Parse(mountainColourStartHeightInput.text);
        Presets.mountainColourEndgreadientHeight = float.Parse(mountainColourEndgreadientHeightInput.text);
        terrainManager.regenerateTerrain();

        Presets.useMarchingCube = UseMarchingcubeInput.isOn;

        showOptions = !showOptions;
        canvas.enabled = showOptions;
    }

    public void randomizeSeed()
    {
        seedInput.text = Random.Range(0, 10000000).ToString();
    }

    public void resetInputs()
    {
        scaleInput.text = Presets.scale.ToString();
        chunkRadiusInput.text = Presets.chunkRadius.ToString();
        ChunkViewRadiusInput.text = Presets.chunksViewRadius.ToString();
        fidelityInput.text = Presets.fidelity.ToString();
        variationInput.text = Presets.variation.ToString();
        seedInput.text = Presets.seed.ToString();
        OctivesInput.text = Presets.octives.ToString();
        OctiveFrequencyInput.text = Presets.octiveRoughnesScaler.ToString();
        octiveScaleInput.text = Presets.octivesScalerScaler.ToString();
        terrainSteepnessInput.text = Presets.terrainSteepness.ToString();
        MarchingCubeGroundHeightInput.text = Presets.marchingCubeGroundHeight.ToString();
        UseMarchingcubeInput.isOn = Presets.useMarchingCube;
        grassColourStartHeightInput.text = Presets.grassColourStartHeight.ToString();
        mountainColourStartHeightInput.text = Presets.mountainColourStartHeight.ToString();
        mountainColourEndgreadientHeightInput.text = Presets.mountainColourEndgreadientHeight.ToString();
    }

    public void regenerateColours()
    {
        Presets.grassColourStartHeight = float.Parse(grassColourStartHeightInput.text);
        Presets.mountainColourStartHeight = float.Parse(mountainColourStartHeightInput.text);
        Presets.mountainColourEndgreadientHeight = float.Parse(mountainColourEndgreadientHeightInput.text);
        terrainManager.regenColours();
    }
    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            showOptions = !showOptions;
            canvas.enabled = showOptions;
        }
    }
}
