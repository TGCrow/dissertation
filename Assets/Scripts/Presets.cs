
public static class Presets
{
    public static int scale = 100;
    public static int chunkRadius = 10;
    public static int chunksViewRadius = 3;
    public static int fidelity = 20; //Higher fidelity = more detail.
    public static float variation = 20;
    public static int seed=432;
    public static int octives = 4;
    public static float octiveRoughnesScaler = 0.3f;
    public static int octivesScalerScaler = 6;
    public static bool useMarchingCube = true;
    public static float terrainSteepness = 2.5f;
    public static float marchingCubeGroundHeight = 0.1f;
    public static float marchingCubeOffsetScale = 5;
    public static float grassColourStartHeight = 0.0f;
    public static float mountainColourStartHeight = 4.0f;
    public static float mountainColourEndgreadientHeight = 8.0f;

}
