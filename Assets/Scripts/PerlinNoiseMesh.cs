using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class PerlinNoiseMesh
{
    public static Vector2[] getNewUvs(Vector3[] verticies)
    {
        List<Vector2> UVs = new List<Vector2>();
        foreach (Vector3 vertex in verticies)
        {
            float height = vertex.y;
            float gradientPos = 0;
            if (height < Presets.mountainColourStartHeight)
            {
                gradientPos = Mathf.Clamp((height - Presets.grassColourStartHeight) / (Presets.mountainColourStartHeight - Presets.grassColourStartHeight), 0.0f, 0.5f);
            }
            else if (height < Presets.mountainColourEndgreadientHeight)
            {
                gradientPos = Mathf.Clamp((height - Presets.mountainColourStartHeight) / (Presets.mountainColourEndgreadientHeight - Presets.mountainColourStartHeight), 0.0f, 0.5f) + 0.49f;
            }
            else
            {
                gradientPos = 0.99f;
            }
            UVs.Add(new Vector2(gradientPos, 0.5f));
        }
        return UVs.ToArray();
    }
    public static Mesh GenerateMesh(Vector3 center)
    {
        Random.InitState(Presets.seed);
        Vector2[] offsets = new Vector2[Presets.octives];

        for (int i = 0; i < Presets.octives; i++)
        {
            int xoffset = Random.Range(-100000, 100000);
            int yoffset = Random.Range(-100000, 100000);
            offsets[i] = new Vector2(xoffset, yoffset);
        }
        
        float distanceBetweenVertex = ((float)Presets.chunkRadius / (float)Presets.fidelity);
        List<Vector3> verticies = new List<Vector3>();
        
        for (float x = -Presets.fidelity; x <= Presets.fidelity; x++)
        {
            for (float z = -Presets.fidelity; z <= Presets.fidelity; z++)
            {
                float xpos = x * distanceBetweenVertex;
                float zpos = z * distanceBetweenVertex;
                float octiveRoughness = Presets.variation;
                int octiveScale = 1;
                float perlinVal =  0.0f;
                for (int i  = 0; i < Presets.octives; i++)
                {
                    perlinVal += Mathf.PerlinNoise((center.x + xpos + offsets[i].x) /octiveRoughness, (center.z + zpos - offsets[i].y) /octiveRoughness)/ (octiveScale);
                    octiveRoughness = octiveRoughness * Presets.octiveRoughnesScaler;
                    octiveScale = octiveScale * Presets.octivesScalerScaler;
                }
                float ypos = Mathf.Pow(perlinVal,Presets.terrainSteepness) * Presets.scale;
                verticies.Add(new Vector3(xpos, ypos, zpos));
            }
        }
        List<Vector2> UVs = new List<Vector2>();
        foreach(Vector3 vertex in verticies)
        {
            float height = vertex.y;
            float gradientPos = 0;
            if (height < Presets.mountainColourStartHeight)
            {
                gradientPos = Mathf.Clamp((height - Presets.grassColourStartHeight) / (Presets.mountainColourStartHeight - Presets.grassColourStartHeight), 0.0f, 0.5f);
            }
            else if (height < Presets.mountainColourEndgreadientHeight)
            {
                gradientPos = Mathf.Clamp((height - Presets.mountainColourStartHeight) / (Presets.mountainColourEndgreadientHeight - Presets.mountainColourStartHeight), 0.0f, 0.5f) + 0.49f;
            }
            else
            {
                gradientPos = 0.99f;
            }
            UVs.Add(new Vector2(gradientPos, 0.5f));
        }

        List<int> triangles = new List<int>();
        int maxI = verticies.Count - (Presets.fidelity * 2) - 1;
        int mod = (Presets.fidelity * 2) + 1;
        for (int i = 0; i < maxI; i++)
        {
            if (i % mod == mod - 1)
            {
                continue;
            }
            triangles.Add(i);
            triangles.Add(i + 1);
            triangles.Add(i + mod);

            triangles.Add(i + 1);
            triangles.Add(i + 1 + mod);
            triangles.Add(i + mod);
        }

        
        
        Mesh mesh = new Mesh();
        mesh.vertices = verticies.ToArray();
        mesh.triangles = triangles.ToArray();
        mesh.uv = UVs.ToArray();
        mesh.RecalculateNormals();
        return mesh;
    }
}
